import Layout from "antd/es/layout";
import React from "react";

import Header from "./components/Header/Header";

function App({ children }) {
  return (
    <Layout>
      <Header />
      <main role="main">{children}</main>
    </Layout>
  );
}

export default App;
