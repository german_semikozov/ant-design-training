import Col from "antd/es/col";
import Row from "antd/es/row";
import Tabs from "antd/es/tabs";
import React from "react";

import styles from "./ContentTabs.module.css";

const { TabPane } = Tabs;

const ContentTabs = ({ items }) => (
  <Tabs
    defaultActiveKey="0"
    tabPosition="top"
    animated={false}
    className={styles.contentTabs}
  >
    {items.map((item, i) => (
      <TabPane tab={item.tabTitle} key={i} className={styles.tabPane}>
        <Row type="flex" justify="center">
          <Col span={24}>{item.tabContent}</Col>
        </Row>
      </TabPane>
    ))}
  </Tabs>
);

export default ContentTabs;
