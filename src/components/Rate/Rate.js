import Rate from "antd/es/rate";
import cx from "classnames";
import React, { useState } from "react";

import styles from "./Rate.module.css";

const RateComponent = ({ amount, isRateDisabled = false }) => {
  const [value, setState] = useState(0);

  const handleChange = value => {
    setState({ value });
  };

  return (
    <>
      <Rate
        disabled={isRateDisabled}
        onChange={handleChange}
        value={value}
        className={cx(styles.customRate, {
          [styles.disabled]: isRateDisabled === true
        })}
      />
      {amount && <span className="ant-rate-text">({amount})</span>}
    </>
  );
};

export default RateComponent;
