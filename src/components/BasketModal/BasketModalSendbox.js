import { Button } from "antd/es";
import React, { useState } from "react";

import BasketModal from "../BasketModal/BasketModal";
import styles from "./BasketModal.module.css";

const BasketModalSendbox = () => {
  const [visible, setVisible] = useState(false);

  const handleClick = () => {
    setVisible(!visible);
  };
  return (
    <div className={styles.mobileVisible}>
      <BasketModal visible={visible} handleClick={handleClick} />
      <Button
        size="large"
        block
        className={styles.fixedBasketModalBtn}
        onClick={handleClick}
      >
        <span>Basket</span>
        <span>€ 8.95</span>
      </Button>
    </div>
  );
};

export default BasketModalSendbox;
