import { Modal } from "antd/es";
import React from "react";

import Basket from "../Basket/Basket";
import styles from "./BasketModal.module.css";

const BasketModal = ({ visible, handleClick }) => (
  <Modal
    visible={visible}
    onCancel={handleClick}
    footer={false}
    className={styles.basketModal}
    width="100%"
  >
    <Basket className={styles.h100} isFlexbox />
  </Modal>
);

export default BasketModal;
