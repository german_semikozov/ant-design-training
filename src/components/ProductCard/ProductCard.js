import Card from "antd/es/card";
import Col from "antd/es/col";
import Row from "antd/es/row";
import React from "react";
import { Link } from "react-router-dom";

import RateComponent from "../Rate/Rate";
import styles from "./ProductCard.module.css";

const ProductCard = ({ productImgSrc, productTitle, productDescription }) => (
  <Link to="product">
    <Card className={styles.productCard} hoverable>
      <Row type="flex" align="top">
        <Col span={24} align="left">
          <div className={styles.imgWrap}>
            <img src={productImgSrc} alt={productTitle} />
          </div>
          <div className={styles.contentWrap}>
            <h4 className={styles.title}>{productTitle}</h4>
            <p className={styles.description}>{productDescription}</p>
            <RateComponent amount="27" />
          </div>
        </Col>
      </Row>
    </Card>
  </Link>
);

export default ProductCard;
