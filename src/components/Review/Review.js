import Card from "antd/es/card";
import Col from "antd/es/col";
import Row from "antd/es/row";
import React from "react";

import RateComponent from "../Rate/Rate";
import styles from "./Review.module.css";

const Review = ({ name, comment }) => (
  <Card className={styles.review}>
    <Row type="flex" align="middle">
      <Col xs={24} md={18} align="left">
        <h4 className={styles.name}>{name}</h4>
        <p className={styles.comment}>{comment}</p>
      </Col>
      <Col xs={8} md={6} align="right" className={styles.rateColumn}>
        <RateComponent isRateDisabled={true} />
      </Col>
    </Row>
  </Card>
);

export default Review;
