import Button from "antd/es/button";
import Card from "antd/es/card";
import Col from "antd/es/col";
import Form from "antd/es/form";
import Input from "antd/es/input";
import Row from "antd/es/row";
import React from "react";

import RateComponent from "../Rate/Rate";
import styles from "./Review.module.css";

const AddReview = ({ title }) => {
  const { TextArea } = Input;
  return (
    <Card className={styles.review}>
      <Row type="flex" align="middle">
        <Col xs={24} md={18} align="left">
          <h4 className={styles.addReviewTitle}>{title}</h4>
          <Form>
            <Form.Item>
              <TextArea
                rows={3}
                size="large"
                autosize={{ minRows: 3, maxRows: 6 }}
              />
            </Form.Item>
            <div>
              Raiting: <RateComponent isRateDisabled={true} />
            </div>
            <Button className={styles.submitButton}>PUBLISH REVIEW</Button>
          </Form>
        </Col>
      </Row>
    </Card>
  );
};

export default AddReview;
