import Button from "antd/es/button";
import Form from "antd/es/form";
import Input from "antd/es/input";
import InputNumber from "antd/es/input-number";
import React from "react";

import styles from "./OrderForm.module.less";

const formItemLayout = {
  labelCol: {
    xs: { span: 24, offset: 0 }
  },
  wrapperCol: {
    xs: { span: 24 }
  }
};

const OrderForm = () => {
  const { TextArea } = Input;
  return (
    <Form {...formItemLayout} className={styles.orderForm}>
      <Form.Item className={styles.formGroup} labelAlign="left" label="Name">
        <Input size="large" placeholder="unavailable choice" />
      </Form.Item>
      <Form.Item
        className={styles.formGroup}
        labelAlign="left"
        label="Phone Number"
      >
        <InputNumber size="large" min={0} style={{ width: "100%" }} />
      </Form.Item>
      <Form.Item className={styles.formGroup} labelAlign="left" label="Address">
        <TextArea
          rows={3}
          size="large"
          autosize={{ minRows: 3, maxRows: 6 }}
          placeholder="textarea"
        />
      </Form.Item>
      <Form.Item>
        <Button type="primary" size="large" block>
          order
        </Button>
      </Form.Item>
    </Form>
  );
};

export default OrderForm;
