import React from "react";

import DefaultHeroImg from "./hero-banner.jpg";
import styles from "./Hero.module.css";

const Hero = ({ img = DefaultHeroImg, heading, description, children }) => (
  <div className={styles.hero}>
    <img src={img} className={styles.heroImg} alt="hero-banner" />
    <div className={styles.heroCaption}>
      <h2 className={styles.heading}>{heading}</h2>
      <p className={styles.description}>{description}</p>
      <div className={styles.colorWhite}>{children}</div>
    </div>
  </div>
);

export default Hero;
