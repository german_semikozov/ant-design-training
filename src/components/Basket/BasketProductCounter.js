import { Button } from "antd/es";
import React, { useState } from "react";

import styles from "./BasketProductCounter.module.css";

const BasketProductCounter = () => {
  const [count, setCount] = useState(0);

  const handleCounterIncrement = () => {
    setCount(count + 1);
  };

  const handleCounterDecrease = () => {
    count > 0 && setCount(count - 1);
  };

  return (
    <div className={styles.counter}>
      <Button
        type="link"
        size="small"
        className={styles.button}
        icon="minus"
        onClick={handleCounterDecrease}
      />
      <span className={styles.count}>{count}</span>
      <Button
        type="link"
        size="small"
        className={styles.button}
        icon="plus"
        onClick={handleCounterIncrement}
      />
    </div>
  );
};

export default BasketProductCounter;
