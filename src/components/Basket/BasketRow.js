import { Col, Row } from "antd/es";
import cx from "classnames";
import React from "react";

import styles from "./Basket.module.css";

const BasketRow = ({ align = "top", leftContent, rightContent, isLast }) => (
  <Row
    type="flex"
    align={align}
    className={cx(styles.basketRow, { [styles.lastRow]: isLast })}
  >
    <Col span={12} align="left">
      {isLast ? <b>{leftContent}</b> : leftContent}
    </Col>
    <Col span={12} align="right">
      {isLast ? <b>{rightContent}</b> : rightContent}
    </Col>
  </Row>
);

export default BasketRow;
