import Button from "antd/es/button";
import cx from "classnames";
import React from "react";

import styles from "./Basket.module.css";
import BasketProductCounter from "./BasketProductCounter";
import BasketRow from "./BasketRow";

const Basket = ({
  title = "Basket",
  hasButton = true,
  className,
  isFlexbox
}) => (
  <div
    className={cx(styles.basket, className, { [styles.isFlexbox]: isFlexbox })}
  >
    <h4 className={styles.title}>{title}</h4>
    <BasketRow
      align="middle"
      leftContent="Product name"
      rightContent={<BasketProductCounter />}
    />
    <hr />
    <BasketRow leftContent="Sub-total" rightContent="€ 8.95" />
    <BasketRow leftContent="Delivery costs" rightContent="FREE" />
    <BasketRow isLast leftContent="Total" rightContent="€ 8.95" />
    {hasButton && (
      <Button type="primary" size="large" block>
        order
      </Button>
    )}
  </div>
);

export default Basket;
