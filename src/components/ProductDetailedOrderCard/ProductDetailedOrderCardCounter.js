import Button from "antd/es/button";
import React, { useState } from "react";

import styles from "./ProductDetailedOrderCardCounter.module.css";

const ProductDetailedOrderCardCounter = () => {
  const [count, setCount] = useState(0);

  const handleCounterIncrement = () => {
    setCount(count + 1);
  };

  const handleCounterDecrease = () => {
    count > 0 && setCount(count - 1);
  };

  return (
    <div className={styles.counter}>
      <div className={styles.count}>{count}</div>
      <Button.Group>
        <Button
          className={styles.button}
          icon="minus"
          onClick={handleCounterDecrease}
        />
        <Button
          className={styles.button}
          icon="plus"
          onClick={handleCounterIncrement}
        />
      </Button.Group>
    </div>
  );
};

export default ProductDetailedOrderCardCounter;
