import Card from "antd/es/card";
import Col from "antd/es/col";
import Row from "antd/es/row";
import React from "react";

import styles from "./ProductDetailedOrderCard.module.css";
import ProductDetailedOrderCardCounter from "./ProductDetailedOrderCardCounter";

const ProductDetailedOrderCard = ({
  productTitle,
  productDescription,
  productPrice
}) => (
  <Card className={styles.productDetailedOrderCard}>
    <Row type="flex" justify="space-between">
      <Col xs={16} md={16} lg={20} align="left">
        <h4 className={styles.title}>{productTitle}</h4>
        <p className={styles.description}>{productDescription}</p>
        <div className={styles.price}>{productPrice}</div>
      </Col>
      <Col xs={8} md={6} lg={4} align="right">
        <ProductDetailedOrderCardCounter />
      </Col>
    </Row>
  </Card>
);

export default ProductDetailedOrderCard;
