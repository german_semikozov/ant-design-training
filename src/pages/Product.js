import Col from "antd/es/col";
import Row from "antd/es/row";
import React from "react";

import Basket from "../components/Basket/Basket";
import BasketModalSendbox from "../components/BasketModal/BasketModalSendbox";
import ContentTabs from "../components/ContentTabs/ContentTabs";
import Hero from "../components/Hero/Hero";
import ProductDetailedOrderCard from "../components/ProductDetailedOrderCard/ProductDetailedOrderCard";
import RateComponent from "../components/Rate/Rate";
import AddReview from "../components/Review/AddReview";
import Review from "../components/Review/Review";
import ProductHeroImg from "../images/product-banner.jpg";

const Reviews = () => (
  <Row type="flex" justify="center" gutter={{ xs: 8, sm: 16, md: 24 }}>
    <Col xs={24} md={16}>
      <Review
        name="Andrew"
        comment="Food was tasty and super friendly delivery! Calls you by first name, gives a nice personal touch. Only downside, sushi quickly fell apart. Tighter roles perhaps?"
      />
      <AddReview title="Leave your review" />
    </Col>
  </Row>
);

const Menu = () => (
  <Row type="flex" justify="center" gutter={{ xs: 8, sm: 16, md: 24 }}>
    <Col xs={24} md={15} lg={12}>
      <ProductDetailedOrderCard
        productTitle="Crispy Ebi"
        productDescription="Fried onions, fried shrimp, avocado, mayonnaise and sesame"
        productPrice="€ 8.95"
      />
    </Col>
    <Col xs={0} md={7} lg={6}>
      <Basket />
    </Col>
  </Row>
);

const items = [
  {
    tabTitle: "Menu",
    tabContent: <Menu />
  },
  {
    tabTitle: "Reviews",
    tabContent: <Reviews />
  }
];

function Product() {
  return (
    <>
      <Hero
        img={ProductHeroImg}
        heading="Heading"
        description="description"
        children={<RateComponent isRateDisabled={true} amount="42" />}
      />
      <ContentTabs items={items} />
      <BasketModalSendbox />
    </>
  );
}

export default Product;
