import Col from "antd/es/col";
import Row from "antd/es/row";
import React from "react";

import ContentTabs from "../components/ContentTabs/ContentTabs";
import Hero from "../components/Hero/Hero";
import ProductCard from "../components/ProductCard/ProductCard";
import ProductImg from "../images/product-img1.png";

const Products = () => (
  <Row type="flex" justify="center" gutter={{ xs: 8, sm: 16, md: 24 }}>
    <Col xs={24} md={16} lg={12}>
      <ProductCard
        productTitle="Maki mix box"
        productDescription="24 pieces"
        productImgSrc={ProductImg}
      />
    </Col>
  </Row>
);

const items = [
  {
    tabTitle: "Everything",
    tabContent: <Products />
  },
  {
    tabTitle: "Italian style pizza",
    tabContent: <Products />
  },
  {
    tabTitle: "Sushi",
    tabContent: <Products />
  },
  {
    tabTitle: "Burgers",
    tabContent: <Products />
  },
  {
    tabTitle: "Chinese",
    tabContent: <Products />
  },
  {
    tabTitle: "Spare ribs",
    tabContent: <Products />
  },
  {
    tabTitle: "Döner",
    tabContent: <Products />
  },
  {
    tabTitle: "Chicken",
    tabContent: <Products />
  }
];

function Home() {
  return (
    <>
      <Hero heading="Heading" />
      <ContentTabs items={items} />
    </>
  );
}

export default Home;
