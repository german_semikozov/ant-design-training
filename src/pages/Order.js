import Col from "antd/es/col";
import Row from "antd/es/row";
import React from "react";

import Basket from "../components/Basket/Basket";
import OrderForm from "../components/OrderForm/OrderForm";

function Order() {
  return (
    <div className="content-wrap">
      <Row type="flex" justify="center" gutter={{ xs: 0, sm: 16, md: 24 }}>
        <Col
          xs={{ span: 24, order: 2 }}
          md={{ span: 10, order: 1 }}
          lg={{ span: 6, order: 1 }}
        >
          <OrderForm />
        </Col>
        <Col
          xs={{ span: 24, order: 1 }}
          md={{ span: 10, order: 2 }}
          lg={{ span: 6, order: 2 }}
        >
          <Basket title="Order" hasButton={false} />
        </Col>
      </Row>
    </div>
  );
}

export default Order;
